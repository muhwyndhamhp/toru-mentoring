package com.toru.toru;

import android.app.Application;
import android.content.Context;

import com.toru.toru.injection.component.ApplicationComponent;
import com.toru.toru.injection.module.ApplicationModule;

import timber.log.Timber;
import com.toru.toru.BuildConfig;
import com.toru.toru.injection.component.DaggerApplicationComponent;

public class ToruApplication extends Application  {

    ApplicationComponent applicationComponent;

    @Override
    public void onCreate() {
        super.onCreate();

        if (BuildConfig.DEBUG) {
            Timber.plant(new Timber.DebugTree());
//            Fabric.with(this, new Crashlytics());
        }
    }

    public static ToruApplication get(Context context) {
        return (ToruApplication) context.getApplicationContext();
    }

    public ApplicationComponent getComponent() {
        if (applicationComponent == null) {
            applicationComponent = DaggerApplicationComponent.builder()
                    .applicationModule(new ApplicationModule(this))
                    .build();
        }
        return applicationComponent;
    }

    // Needed to replace the component with a test specific one
    public void setComponent(ApplicationComponent applicationComponent) {
        this.applicationComponent = applicationComponent;
    }
}
