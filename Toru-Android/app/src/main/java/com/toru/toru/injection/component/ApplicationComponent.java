package com.toru.toru.injection.component;

import android.app.Application;
import android.content.Context;

import com.toru.toru.data.DataManager;
import com.toru.toru.data.SyncService;
import com.toru.toru.data.local.DatabaseHelper;
import com.toru.toru.data.local.PreferencesHelper;
import com.toru.toru.data.remote.RibotsService;
import com.toru.toru.injection.ApplicationContext;
import com.toru.toru.injection.module.ApplicationModule;
import com.toru.toru.util.RxEventBus;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = ApplicationModule.class)
public interface ApplicationComponent {

    void inject(SyncService syncService);

    @ApplicationContext
    Context context();
    Application application();
    RibotsService ribotsService();
    PreferencesHelper preferencesHelper();
    DatabaseHelper databaseHelper();
    DataManager dataManager();
    RxEventBus eventBus();

}
