package com.toru.toru.injection.component;

import com.toru.toru.injection.PerActivity;
import com.toru.toru.injection.module.ActivityModule;
import com.toru.toru.ui.main.MainActivity;

import dagger.Subcomponent;

/**
 * This component inject dependencies to all Activities across the application
 */
@PerActivity
@Subcomponent(modules = ActivityModule.class)
public interface ActivityComponent {

    void inject(MainActivity mainActivity);

}
